﻿using MassTransit;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.Contracts;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Serilog;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services
{
    public class PromoCodeService
    {
        public async Task ProcessPromoCodeReceived(HttpContext context, PromoCode promoCode)
        {
            try
            {
                var _publishEndpoint = context.RequestServices.GetService<IPublishEndpoint>();
                await _publishEndpoint.Publish(new PromoCodeCreated
                {
                    PromoCodeJson = JsonConvert.SerializeObject(promoCode)
                });
                Log.Information($"Создание промокодов: сообщение PromoCodeReceivedEvent успешно отправлено в очереди");
            }
            catch (Exception ex) {
                Log.Information($"Создание промокодов: сообщение не отправлено в очереди, ошибка {ex.Message}");
            }
            
        }

        /*public async Task ProcessPromoCodeReceived(IServiceProvider serviceProvider, PromoCode promoCode)
        var publishEndpoint = serviceProvider.GetRequiredService<IPublishEndpoint>();
        var jsonSettings = new JsonSerializerSettings
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        };
        await publishEndpoint.Publish(new PromoCodeReceivedEvent
        {
            PromoCodeJson = JsonConvert.SerializeObject(promoCode, jsonSettings)
        });
        Log.Information($"Создание промокодов: сообщение PromoCodeReceivedEvent добавлено в очередь");*/
    }
}
