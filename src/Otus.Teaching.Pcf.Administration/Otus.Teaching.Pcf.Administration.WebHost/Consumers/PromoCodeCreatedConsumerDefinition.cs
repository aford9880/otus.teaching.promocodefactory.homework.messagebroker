﻿using MassTransit;
using Otus.Teaching.Pcf.Contracts;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class PromoCodeCreatedConsumerDefinition : ConsumerDefinition<PromoCodeCreatedConsumer>
    {
        public PromoCodeCreatedConsumerDefinition()
        {
            EndpointName = Constants.NotificationQueueNameAdministration;
        }

        protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<PromoCodeCreatedConsumer> consumerConfigurator)
        {
            endpointConfigurator.UseRetry(x => x.Intervals(100, 500, 1000));
        }
    }
}
