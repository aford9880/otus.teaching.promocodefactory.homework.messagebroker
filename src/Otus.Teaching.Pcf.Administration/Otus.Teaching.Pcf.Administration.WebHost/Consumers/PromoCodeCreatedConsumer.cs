﻿using MassTransit;
using Newtonsoft.Json.Linq;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.WebHost.Services;
using Otus.Teaching.Pcf.Contracts;
using Serilog;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class PromoCodeCreatedConsumer : IConsumer<IPromoCodeCreated>
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly PromoCodeService _promoCodeService;

        public PromoCodeCreatedConsumer(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
            _promoCodeService = new PromoCodeService(employeeRepository);
        }

        public async Task Consume(ConsumeContext<IPromoCodeCreated> context)
        {
            Log.Information("Администратор: получено сообщение PromoCodeCreated");
            try
            {
                var @event = context.Message;
                JObject json = JObject.Parse(@event.PromoCodeJson);
                var partnerId = json["PartnerManagerId"]?.ToString();
                if (partnerId == null)
                    throw new Exception("ID сотрудника не получен!");
                Guid partnerIdGuid = Guid.Parse(partnerId);

                await _promoCodeService.ApplyPromoCodeCountAsync(partnerIdGuid);
                await Task.CompletedTask;
            }
            catch (Exception ex)
            {
                Log.Information($"Администратор: ошибка обработки сообщения {ex.Message}");
            }
        }
    }
}
