﻿using Serilog;
using System.Threading.Tasks;
using System;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{
    public class PromoCodeService
    {
        private readonly IRepository<Employee> _employeeRepository;

        public PromoCodeService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task<Employee> ApplyPromoCodeCountAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                throw new Exception("Сотрудник не найден!");

            employee.AppliedPromocodesCount++;
            await _employeeRepository.UpdateAsync(employee);
            Log.Information($"Администратор: сообщение PromoCodeReceivedEvent успешно обработано, количество выданных промокодов у сотрудника {employee.FullName} увеличено");

            return employee;
        }
    }
}
