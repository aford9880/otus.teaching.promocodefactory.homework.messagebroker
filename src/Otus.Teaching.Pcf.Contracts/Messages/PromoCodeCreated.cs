﻿using Otus.Teaching.Pcf.Contracts.Abstractions;

namespace Otus.Teaching.Pcf.Contracts.Messages
{
    public class PromoCodeCreated : IPromoCodeCreated
    {
        public string PromoCodeJson { get; set; }
    }
}
