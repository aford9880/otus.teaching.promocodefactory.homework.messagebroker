﻿namespace Otus.Teaching.Pcf.Contracts.Messages
{
    public static class Constants
    {
        public static string NotificationQueueNameAdministration = "NotificationQueueAdministration";
        public static string NotificationQueueNameGivingToCustomer = "NotificationQueueGivingToCustomer";
    }
}
