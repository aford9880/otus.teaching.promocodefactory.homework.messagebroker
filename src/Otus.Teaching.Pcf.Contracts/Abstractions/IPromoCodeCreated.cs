﻿namespace Otus.Teaching.Pcf.Contracts.Abstractions
{
    public interface IPromoCodeCreated
    {
        string PromoCodeJson { get; set; }
    }
}
