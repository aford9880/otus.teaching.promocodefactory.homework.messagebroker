﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Serilog;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services
{
    public class PromoCodeService
    {
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer>_customersRepository;
        private readonly IRepository<PromoCode>_promoCodesRepository;

        public PromoCodeService(IRepository<Preference> preferencesRepository, IRepository<Customer> customersRepository, IRepository<PromoCode> promoCodesRepository)
        {
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
            _promoCodesRepository = promoCodesRepository;
        }

        public async Task<PromoCode> CreatePromoCodeAsync(GivePromoCodeRequest request, Guid preferenceId)
        {
            var preference = await _preferencesRepository.GetByIdAsync(preferenceId);
            if (preference == null)
            {
                throw new ArgumentException("Предпочтение не найдено!");
            }

            var customers = await _customersRepository.GetWhere(d => d.Preferences.Any(x => x.Preference.Id == preferenceId));

            var promocode = new PromoCode
            {
                Id = request.PromoCodeId,
                PartnerId = request.PartnerId,
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                BeginDate = DateTime.Parse(request.BeginDate).ToUniversalTime(),
                EndDate = DateTime.Parse(request.EndDate).ToUniversalTime(),
                Preference = preference,
                PreferenceId = preference.Id,
                Customers = customers.Select(item => new PromoCodeCustomer
                {
                    CustomerId = item.Id,
                    Customer = item,
                    PromoCodeId = request.PromoCodeId,
                    PromoCode = null
                }).ToList()
            };

            await _promoCodesRepository.AddAsync(promocode);

            Log.Information($"Раздача промокодов: сообщение PromoCodeReceivedEvent успешно обработано, промокоды получили {customers.Count()} клиентов");

            return promocode;
        }
    }
}
