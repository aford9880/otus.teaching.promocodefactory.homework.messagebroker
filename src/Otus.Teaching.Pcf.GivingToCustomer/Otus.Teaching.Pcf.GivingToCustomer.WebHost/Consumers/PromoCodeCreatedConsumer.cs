﻿using MassTransit;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.Contracts;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class PromoCodeCreatedConsumer : IConsumer<IPromoCodeCreated>
    {
        private readonly IRepository<Customer> _customersRepository;
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly PromoCodeService _promoCodeService;

        public PromoCodeCreatedConsumer(IRepository<Customer> customersRepository, IRepository<PromoCode> promoCodesRepository, IRepository<Preference> preferencesRepository)
        {
            _customersRepository = customersRepository;
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _promoCodeService = new PromoCodeService(preferencesRepository, customersRepository, promoCodesRepository);
            
        }

        public async Task Consume(ConsumeContext<IPromoCodeCreated> context)
        {
            var @event = context.Message;
            Log.Information($"Раздача промокодов: сообщение PromoCodeReceivedEvent получено");
            PromoCode promoCodeFromPartner = JsonConvert.DeserializeObject<PromoCode>(@event.PromoCodeJson);

            var request = new GivePromoCodeRequest
            {
                PromoCodeId = promoCodeFromPartner.Id,
                PartnerId = promoCodeFromPartner.PartnerId,
                PromoCode = promoCodeFromPartner.Code,
                ServiceInfo = promoCodeFromPartner.ServiceInfo,
                BeginDate = promoCodeFromPartner.BeginDate.ToString(),
                EndDate = promoCodeFromPartner.EndDate.ToString(),
                PreferenceId = promoCodeFromPartner.PreferenceId
            };

            await _promoCodeService.CreatePromoCodeAsync(request, promoCodeFromPartner.PreferenceId);
            


            await Task.CompletedTask;
        }
    }
}
